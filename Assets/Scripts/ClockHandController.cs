using UnityEngine;
using UnityEngine.EventSystems;

public class ClockHandController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [SerializeField]
    protected int rotationAngle;

    private Transform pivot;
    protected Vector2 prevUp;
    protected int prevValue;

    public delegate void OnPositionChanged(int value);
    public OnPositionChanged onPositionChanged;

    public void OnPointerDown(PointerEventData eventData)
    {

    }
    virtual public void OnPointerUp(PointerEventData eventData)
    {
        Vector2 pos = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.localPosition);

        var angle = Mathf.Acos(Vector2.Dot(Vector2.up, transform.up) / (Vector2.up.magnitude * transform.up.magnitude));

        angle = angle * Mathf.Rad2Deg;

        if (pos.x < Vector2.up.x)
        {
            angle = 360 - angle;
        }

        int rotDir = RotationDirection();  
        var value = Mathf.RoundToInt(angle / rotationAngle);

        onPositionChanged?.Invoke(value);
        
        prevUp = transform.up;
        prevValue = value;
    }

    //Checks for rotation direction: -1 is counterclockwise, 1 is right clockwise;
    protected int RotationDirection()
    {
        Vector3 cross = Vector3.Cross(prevUp, transform.up);
        int res = 0;

        if (Vector3.Dot(cross, Vector3.forward) > 0.0)
        {
            res = -1;
        }
        else
        {
            res = 1;
        }
        return res;
    }

    void Start()
    {
        prevValue = 0;
        prevUp = transform.up;
        pivot = transform;
        transform.parent = pivot;
    }

    public void UpdateValue(int value)
    {
        int angle = -rotationAngle * value;
        transform.localRotation = Quaternion.Euler(0, 0, angle);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 orbVector = Camera.main.WorldToScreenPoint(transform.position);
        orbVector = Input.mousePosition - orbVector;
        var angle = Mathf.Atan2(orbVector.y, orbVector.x) * Mathf.Rad2Deg;
        pivot.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }
}

using UnityEngine;
using UnityEngine.EventSystems;

public class HourHandController : ClockHandController
{
    private bool afterMidnight = true;
    public override void OnPointerUp(PointerEventData eventData)
    {
        Vector2 pos = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.localPosition);

        var angle = Mathf.Acos(Vector2.Dot(Vector2.up, transform.up) / (Vector2.up.magnitude * transform.up.magnitude)) * Mathf.Rad2Deg;

        if (pos.x < Vector2.up.x)
            angle = 360 - angle;

        int rotDir = RotationDirection();
        var value = Mathf.RoundToInt(angle / rotationAngle);

        if (value == 12)
            value = 0;

        if ((rotDir > 0 && prevValue > value) || (rotDir < 0 && prevValue < value))
        {
            afterMidnight = !afterMidnight;
        }

        var ret = value;
        if (!afterMidnight)
            ret += 12;

        onPositionChanged?.Invoke(ret);

        prevUp = transform.up;
        prevValue = value;
    }
}

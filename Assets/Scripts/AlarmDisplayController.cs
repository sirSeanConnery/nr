using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AlarmDisplayController : MonoBehaviour
{
    private TextMeshProUGUI text;
    private Color activeColor;
    private bool updateNeeded = false;
    private bool isActive = false;
    private int hours;
    private int minutes;

    private void Start()
    {
        activeColor = new Color(39, 191, 0, 255);
        text = transform.GetComponent<TextMeshProUGUI>();
        if (!text)
        {
            Debug.LogError("Couldnt find TMP component on AlarmDisplay");
        }

        AlarmSetupController.onAlarmSetup += UpdateDisplay;
        text.faceColor = Color.gray;
    }

    private void FixedUpdate()
    {
        if (updateNeeded) 
        {
            if (isActive)
            {
                text.SetText(string.Format("{0} : {1}", hours.ToString(), minutes.ToString()));
                text.faceColor = activeColor;
            } 
            else
            {
                text.SetText("-- : --");
                text.faceColor = Color.gray;
            }
        }
    }

    private void OnEnable()
    {
        AlarmSetupController.onAlarmSetup += UpdateDisplay;
    }

    private void OnDisable()
    {
        AlarmSetupController.onAlarmSetup -= UpdateDisplay;
    }

    void UpdateDisplay(int HH, int MM, bool state)
    {
        hours = HH;
        minutes = MM;
        isActive = state;
        updateNeeded = true;
    }
}

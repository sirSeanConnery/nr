using System;
using UnityEngine;

public class AnalogClockController : MonoBehaviour
{
    [SerializeField]
    private Transform hourHand;
    [SerializeField]
    private Transform minuteHand;
    [SerializeField]
    private Transform secondsHand;

    private DateTime currentTime;

    void Awake()
    {
        InternalClock.onTimeUpdated += UpdateTime;
    }

    private void OnDisable()
    {
        InternalClock.onTimeUpdated -= UpdateTime;
    }
    private void FixedUpdate()
    {
        int sec = -6 * currentTime.Second;
        secondsHand.localRotation = Quaternion.Euler(0, 0, sec);

        int min = -6 * currentTime.Minute;
        minuteHand.localRotation = Quaternion.Euler(0, 0, min);

        int hour = -30 * currentTime.Hour;
        hourHand.localRotation = Quaternion.Euler(0, 0, hour);
    }

    void UpdateTime(DateTime dateTime)
    {
        currentTime = dateTime;
    }
}

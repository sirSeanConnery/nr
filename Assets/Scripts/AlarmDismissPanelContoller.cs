using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmDismissPanelContoller : MonoBehaviour
{
    [SerializeField]
    GameObject dismissPanel;

    void Awake()
    {
        ClockAlarm.onAlarmActivated += DisplayPanel;
    }

    private void OnDisable()
    {
        ClockAlarm.onAlarmActivated -= DisplayPanel;
    }

    void DisplayPanel()
    {
        if(dismissPanel)
            dismissPanel.SetActive(true);
    }
}

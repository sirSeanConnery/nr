using UnityEngine;
using System;
using TMPro;

public class DigitalClockController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI text;
    private string currentTime;

    void Awake()
    {
        InternalClock.onTimeUpdated += UpdateTime;
    }

    private void OnDisable()
    {
        InternalClock.onTimeUpdated -= UpdateTime;
    }
    private void FixedUpdate()
    {
        text.SetText(currentTime);
    }

    void UpdateTime(DateTime dateTime)
    {
        currentTime = String.Format("{0:HH:mm:ss}", dateTime);
    }
}

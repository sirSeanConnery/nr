using UnityEngine;
using UnityEngine.UI;
using System;

public class AlarmSetupController : MonoBehaviour
{
    [SerializeField]
    InputField hourInput;

    [SerializeField]
    InputField minuteInput;

    [SerializeField]
    ClockHandController hourHandContoller;

    [SerializeField]
    ClockHandController minuteHandContoller;

    [SerializeField]
    Slider alarmActivation;

    private int hours;
    private int minutes;
    private int minValue = 0;
    private int maxHour = 23;
    private int maxMinutes = 59;

    public delegate void OnAlarmSetup(int hours, int minutes, bool isActive);
    public static OnAlarmSetup onAlarmSetup;

    public void Start()
    {
        if (hourInput)
        {
            hourInput.onEndEdit.AddListener(delegate
            {
                System.Int32.TryParse(hourInput.text, out int value);
                ValidateClockIntInput(ref hourInput, ref value, minValue, maxHour);
                hours = value;
                hourHandContoller.UpdateValue(value);
            });
            UpdateHourValue(0);

        }

        if (minuteInput)
        {
            minuteInput.onEndEdit.AddListener(delegate
            {
                System.Int32.TryParse(minuteInput.text, out int value);
                ValidateClockIntInput(ref minuteInput, ref value, minValue, maxMinutes);
                minutes = value;
                minuteHandContoller.UpdateValue(value);
            });
            UpdateMinuteValue(0);
        }
    }

    private void OnEnable()
    {
        hourHandContoller.onPositionChanged += UpdateHourValue;
        minuteHandContoller.onPositionChanged += UpdateMinuteValue;
    }

    private void OnDisable()
    {
        hourHandContoller.onPositionChanged -= UpdateHourValue;
        minuteHandContoller.onPositionChanged -= UpdateMinuteValue;
    }

    public void SetupAlarm()
    {
        bool val = System.Convert.ToBoolean(alarmActivation.value);
        onAlarmSetup?.Invoke(hours, minutes, val);
    }

    public void UpdateUIState() 
    {
        alarmActivation.value = 0;
    }

    void UpdateHourValue(int val)
    {
        System.Int32.TryParse(hourInput.text, out int value);
        value = val;
        ValidateClockIntInput(ref hourInput, ref value, minValue, maxHour);
        hours = value;
        hourHandContoller.UpdateValue(value);
    }
    void UpdateMinuteValue(int val)
    {
        System.Int32.TryParse(minuteInput.text, out int value);
        value = val;
        ValidateClockIntInput(ref minuteInput, ref value, minValue, maxMinutes);
        minutes = value;
        minuteHandContoller.UpdateValue(value);
    }

    void ValidateClockIntInput(ref InputField input, ref int value, int minValue, int maxValue)
    {
        if (value > maxValue)
        {
            value = maxValue;
        }
        else if (value <= minValue)
        {
            value = minValue;
        }

        string validatedText = value.ToString();

        if (value < 10)
        {
            validatedText = '0' + validatedText;
        }

        input.text = validatedText;
    }
}

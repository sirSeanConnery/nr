using Defective.JSON;
using System;
using System.Collections;
using System.Timers;
using UnityEngine;
using UnityEngine.Networking;
public class InternalClock : MonoBehaviour
{
    private int oneSecond = 1000;
    private int oneHour = 3600000;
    private DateTime currentTime;
    private System.Timers.Timer oneSecondTick;
    private System.Timers.Timer hourlyUpdateTick;

    public delegate void OnTimeUpdated(DateTime dateTime);
    public static OnTimeUpdated onTimeUpdated;

    // Start is called before the first frame update
    void Start()
    {
        //initialize with system time for smoother experience
        currentTime = DateTime.Now;

        //Get current Time from TimeServices
        GetCurrentTime();

        //Setup timers
        oneSecondTick = new System.Timers.Timer(oneSecond);
        oneSecondTick.Elapsed += OnSecondElapsed;
        oneSecondTick.AutoReset = true;
        oneSecondTick.Enabled = true;

        hourlyUpdateTick = new System.Timers.Timer(oneHour);
        hourlyUpdateTick.Elapsed += OnHourElapsed;
        hourlyUpdateTick.AutoReset = true;
        hourlyUpdateTick.Enabled = true;
    }

    private void OnDisable()
    {
        oneSecondTick?.Stop();
        hourlyUpdateTick?.Stop();
    }

    private void OnSecondElapsed(System.Object source, ElapsedEventArgs e)
    {
        currentTime = currentTime.AddSeconds(1);
        onTimeUpdated?.Invoke(currentTime);
    }
    private void OnHourElapsed(System.Object source, ElapsedEventArgs e)
    {
        //Update Time Hourly
        GetCurrentTime();
    }

    IEnumerator GetTimeFromURL(string url, string fieldName)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.result == UnityWebRequest.Result.Success)
            {
                JSONObject json = new JSONObject(webRequest.downloadHandler.text);
                var datetime = json.GetField(fieldName);
                if (!DateTime.TryParse(datetime.stringValue, out currentTime))
                {
                    Debug.Log("Error parsing data from " + webRequest.url);
                }
            }
            else
            {
                Debug.Log(webRequest.error);
            }
        }
    }
    void GetCurrentTime()
    {
        //Worldclockapi.com provide datetime without seconds
        StartCoroutine(GetTimeFromURL("http://worldclockapi.com/api/json/utc/now", "currentDateTime"));
        StartCoroutine(GetTimeFromURL("http://worldtimeapi.org/api/ip", "datetime"));
    }
}

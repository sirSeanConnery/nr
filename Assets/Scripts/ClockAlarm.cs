using System;
using System.Collections;
using UnityEngine;

public class ClockAlarm : MonoBehaviour
{
    private DateTime alarmTime;
    private bool isAlarmTriggered = false;
    private bool isAlarmSetUp = false;
    TimeSpan timediffrence;
    
    AudioSource audioSource;

    public delegate void OnAlarmActivated();
    public static OnAlarmActivated onAlarmActivated;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        timediffrence = new TimeSpan(0, 0, 0, 1);
    }
    void Awake()
    {
        InternalClock.onTimeUpdated += CheckAlarm;
        AlarmSetupController.onAlarmSetup += SetAlarm;
    }

    private void OnDisable()
    {
        InternalClock.onTimeUpdated -= CheckAlarm;
        AlarmSetupController.onAlarmSetup -= SetAlarm;
    }
    private void FixedUpdate()
    {
        if (isAlarmTriggered)
        {
            isAlarmSetUp = false;
            isAlarmTriggered = false;

            PlaySound();
            onAlarmActivated?.Invoke();
        }
    }

    void CheckAlarm(DateTime dateTime)
    {
        var diff = dateTime.TimeOfDay - alarmTime.TimeOfDay;
        if (isAlarmSetUp && diff >= TimeSpan.Zero && diff < timediffrence && !isAlarmTriggered)
        {
            isAlarmTriggered = true;
        }
    }

    void SetAlarm(int hours, int minutes, bool isActive)
    {
        alarmTime = alarmTime.Date.AddHours(hours).AddMinutes(minutes);
        isAlarmSetUp = isActive;
    }

    void PlaySound()
    {
        if (audioSource)
        {
            audioSource.Play();           
        }
    }

    public void DissmissAlarm()
    {
        isAlarmSetUp = false;
        isAlarmTriggered = false;
        audioSource.Stop();
    }
}
